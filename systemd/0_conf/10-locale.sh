# Calculate exec=/bin/bash mergepkg(sys-apps/systemd)!=

if ! localectl | grep "X11 Layout: pt" >/dev/null; then
    echo "enabling x11 keymap pt"
    localectl set-x11-keymap pt
fi
if ! localectl | grep "VC Keymap: pt-latin9" >/dev/null; then
    echo "enabling keymap pt-latin9"
    localectl set-keymap pt-latin9
fi
