# Calculate exec=/bin/bash mergepkg(app-eselect/eselect-fontconfig)!=

echo "configuring font hinting"
if [ ! -e /etc/fonts/conf.d/10-hinting-none.conf ]; then
    eselect fontconfig enable 6
fi
if [ -e /etc/fonts/conf.d/10-hinting-slight.conf ]; then
    eselect fontconfig disable 7
fi
if [ ! -e /etc/fonts/conf.d/10-sub-pixel-rgb.conf ]; then
    eselect fontconfig enable 11
fi
if [ ! -e /etc/fonts/conf.d/11-lcdfilter-default.conf ]; then
    eselect fontconfig enable 15
fi
if [ ! -e /etc/fonts/conf.d/50-user.conf ]; then
    eselect fontconfig enable 30
fi
